from openpyxl import Workbook

workbook_obj = Workbook()
active_sheet = workbook_obj.active

rows = (
    (10, 20, "Yaadav"),
    ("Anupam", 56, 64),
    (11, 13, 88)
)

for row in rows:
    active_sheet.append(row)

workbook_obj.save("appendedExcelFile.xlsx")
