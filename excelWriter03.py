from openpyxl import Workbook

workbook_obj = Workbook()

active_sheet = workbook_obj.active
print(active_sheet)

workbook_obj.create_sheet("testing") # To create a sheet
print(workbook_obj.sheetnames)

del workbook_obj["Sheet"] # To delete a sheet
print(workbook_obj.sheetnames)

workbook_obj.create_sheet("QA", 0) # To create a sheet at specific position.
print(workbook_obj.sheetnames)

workbook_obj.save("Wexample03.xlsx")
