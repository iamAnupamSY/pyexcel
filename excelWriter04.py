from openpyxl import Workbook
from openpyxl.styles import Alignment

workbook_obj = Workbook()
active_sheet = workbook_obj.active

active_sheet.merge_cells("A1:E1")

merged_cell = active_sheet.cell(row=1, column=1)
merged_cell.value = "Heading line"
merged_cell.alignment = Alignment(horizontal="center", vertical="center")

workbook_obj.save("cellMerging.xlsx")
