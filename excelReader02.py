import openpyxl

workbook_obj = openpyxl.load_workbook("appendedExcelFile.xlsx")
active_sheet = workbook_obj.active

def slicingEg01():
    rows = active_sheet["A1" : "B3"]
    for col1, col2 in rows:
        print(col1.value, col2.value)

def slicingEg02():
    rows = active_sheet["A1" : "B1"]
    for col1, col2 in rows:
        print(col1.value, col2.value)

def slicingEg03():
    rows = active_sheet["A1" : "C2"]
    for col1, col2, col3 in rows:
        print(col1.value, col2.value, col3.value)


def main():
    slicingEg01()
    print("---------------")
    slicingEg02()
    print("---------------")
    slicingEg03()

main()
