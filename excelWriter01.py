from openpyxl import Workbook # From the openpyxl module, we import the Workbook class

workbook_obj = Workbook() # We create an instance of Workbook class
active_sheet = workbook_obj.active # A workbook is always created with at least one worksheet (ie. "Sheet").

active_sheet["A1"] = 56
active_sheet["A2"] = 43
active_sheet.cell(row=1, column=2).value = 40
active_sheet.cell(row=2, column=2).value = 77

workbook_obj.save("Wexample.xlsx") # We write the contents to the file with the save() method.
