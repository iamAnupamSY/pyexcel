from openpyxl import load_workbook

workbook_obj = load_workbook("appendedExcelFile.xlsx")

active_sheet = workbook_obj.active

A1 = active_sheet["A1"]
A2 = active_sheet["A2"]
B1 = active_sheet.cell(row=1, column=2)
B2 = active_sheet.cell(row=2, column=2)

print(A1.value)
print(A2.value) 
print(B1.value)
print(B2.value)
